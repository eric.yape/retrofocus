FROM centos:centos7

LABEL description="Retrofocus" maintainer=eric.yape@gmail.com version="1.1"

RUN yum update -y
RUN yum install -y epel-release
RUN yum update -y
RUN yum install -y httpd php php-pdo php-xml perl-Image-ExifTool unzip

WORKDIR /etc/httpd/conf.d
COPY Retrofocus.conf .

RUN mkdir -p /var/www/html/Retrofocus/Aperture

WORKDIR /var/www/html/Retrofocus
RUN mkdir css images js
COPY html/css/* css/
COPY html/images/* images/
COPY html/js/* js/
COPY html/index.php .

WORKDIR /usr/local
COPY CodeIgniter-3.1.11.zip .
RUN unzip CodeIgniter-3.1.11.zip
RUN ln -s CodeIgniter-3.1.11 CI3
RUN rm CodeIgniter-3.1.11.zip

WORKDIR /usr/local/CI3/application/libraries
COPY CFPropertyList.zip .
RUN unzip CFPropertyList.zip
RUN rm CFPropertyList.zip

RUN mkdir /usr/local/CI3/application/views/Retrofocus
COPY models/*.php /usr/local/CI3/application/models/
COPY views/Retrofocus/*.php /usr/local/CI3/application/views/Retrofocus/
COPY views/templates/*.php /usr/local/CI3/application/views/templates/
COPY controllers/*.php /usr/local/CI3/application/controllers/
COPY config/*.php /usr/local/CI3/application/config/
COPY libraries/*.php /usr/local/CI3/application/libraries/

EXPOSE 80
ENTRYPOINT ["/usr/sbin/httpd", "-D", "FOREGROUND"]
