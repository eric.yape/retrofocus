# Retrofocus

![Retrofocus logo](./screenshots/retrofocus-logo-small.png)

**Retrofocus** is a lightweight web application that enables you to browse your Apple **Aperture** photo collection **on many platforms** ! Images modified in **Aperture** will not be rendered by **Retrofocus** but you will be able to retrieves all the adjustments as a list of properties.

## Introduction

A (very) long time ago, Apple was a company that did not exclusively concentrate on selling pricey smartphones. It was also developping and selling one of the greatest software application for pro and enthousiastic photographers: Aperture.
Aperture would allow any photographers to manage huge collections of images in a fast and intuitive manner. Plus, it would allow non destructive raw editing with powerful sets of tools and adjustments.

Unfortunately, Apple made the decision to stop the support for this marvellous piece of software in november 2013 and since then, many photographers feel abandonned. Nowadays latest MacOs releases are not compatible with Aperture.

That's the rationale of the **Retrofocus** application: it allows eternal Aperture aficionados to still **enjoy** their vast **Aperture** photo collections with a simple **web browser** !

## Description

Retrofocus is a web application based on the [CodeIgniter](https://codeigniter.com/) V3.1 framework that enables browsing of Apple Aperture photo collections.

Retrofocus can not edit nor add new images to Aperture photo collections : it's a read-only viewer that do not alter the original photo collections. You may use it safely (but at your own risks) on your precious photo libraries.

Retrofocus is natively packaged as a [Docker](https://www.docker.com/) application, so that you can build it and use it very easily on many platforms. However, as a very basic CodeIgniter application, you may also easily review and edit the Retrofocus PHP code to adapt it to your needs.

## Release notes

| Versions | Dates | Short descriptions |
| ---------- | ---------- | ---------- |
| V1.0 | April 11 2020 | Initial release |
| V1.1 | April 26 2020 | Aperture properties management; "Project" view new layout |

## User manual

### Installation

There is 2 possible ways to install Retrofocus:
1. with Docker: easy, but you need first to set up [Docker](https://www.docker.com/get-started)
1. into an existing CodeIgniter framework: [also really simple](https://codeigniter.com/userguide3/index.html) !

Choose the one you feels the most comfortable with !

#### Building the docker Retrofocus application

- Fetch the entire Retrofocus files into your local folder:
```
git clone https://gitlab.com/eric.yape/retrofocus.git
```
- Build the application with `docker build`:
```
cd retrofocus
docker build . -t retrofocus
```
- Use `docker run` to start Retrofocus (see below for exact command line parameters)

#### Deploying Retrofocus in an existing CodeIgniter framework

- Fetch the entire Retrofocus files into your local folder:
```
git clone https://gitlab.com/eric.yape/retrofocus.git
```
- Read the `Dockerfile` file to know where to put Retrofocus HTML and PHP files in the CodeIgniter MVC tree hierarchy. Carefully merge Retrofocus `routes.php`, `database.php` and `constants.php` files to the existing ones
- Install the `exiftool` program

### Connecting the Aperture photo collection to Retrofocus

Retrofocus needs to know where the photo collection is stored !

Briefly explained:

1. If you have previously built the docker application you just need to specify this parameter in the `-v` parameter:
```
docker run --rm  -v /mnt/RetrofocusImages:/var/www/html/Retrofocus/Aperture -p 8080:80 retrofocus
```
where `/mnt/RetrofocusImages` is the folder where `Aperture Library.aplibrary` is stored (NB: not the `Aperture Library.aplibrary` folder itself: it's **parent**)

2. If you reuse the Retrofocus PHP code in a different Codeigniter framework you need to edit the Retrofocus `constants.php` file in the `config` CodeIgniter folder

Step-by-step and detailed explanations:

#### Use case 1: connecting the Aperture photo collection to Retrofocus on Linux/docker with a Samba share

![Use case 1](./screenshots/use-case-1.png)

If your Aperture photo collection is stored on an external disk attached to your Mac or stored in an internal Mac folder you can use a Samba share to browse it with Retrofocus running on Linux (if the two computers are on the same network):

- On the Mac:
 - Set up a [Samba share](https://support.apple.com/guide/mac-help/share-mac-files-with-windows-users-mchlp1657/mac) for the `Aperture Library.aplibrary` **parent** folder (name it `RemoteApertureImages`). During this step you may create a dedicated user with read-only privileges on this shared folder (e.g. `samba`)
- On the Linux computer:
 - Check that [mount.cifs](https://linuxize.com/post/how-to-mount-cifs-windows-share-on-linux/) is available
 - Create a first folder that will be used as a mount point: `sudo mkdir /mnt/LocalApertureImages`
 - Create a second folder that will be used by Retrofocus: `sudo mkdir /mnt/RetrofocusImages`
 - Mount the shared folder with the `mount` command: `sudo mount -t cifs -o user=samba //192.168.0.99/RemoteApertureImages /mnt/LocalApertureImages` (replace `192.168.0.99` with the actual IP address of your Mac)
 - Remap the mounted folder with the [bindfs command](https://bindfs.org/): `sudo bindfs --map=99/48:@99/@48 /mnt/LocalApertureImages /mnt/RetrofocusImages`
 - `48` and `@48` are the user and group ids of the **apache** Linux user (check this with `id apache`)
 - `99` and `@99` are the user and group ids of the `nobody` default HFS user (check this with `id nobody`)
 - run docker with the `-v` parameter: `docker run --rm  -v /mnt/RetrofocusImages:/var/www/html/Retrofocus/Aperture -p 8080:80 retrofocus`

#### Use case 2: connecting the Aperture photo collection to Retrofocus on Linux/docker by reusing an external Mac disk

 ![Use case 2](./screenshots/use-case-2.png)

 If you can connect an external disk that has been used to store your Aperture photo collection to your Linux computer, you will get better performance:
- On the Linux computer:
 - Check that [HFSPlus](https://forums.centos.org/viewtopic.php?t=67360) is available on your Linux box
 - Create a first folder that will be used as a mount point: `sudo mkdir /mnt/LocalApertureImages`
 - Create a second folder that will be used by Retrofocus: `sudo mkdir /mnt/RetrofocusImages`
 - Display the external HFS disk device name with `blkid | grep hfs`
 - Mount the disk with the `mount` command: `sudo mount -t hfsplus /dev/sdXX /mnt/LocalApertureImages` (replace `/dev/sdXX` with the actual device name displayed at the previous step)
 - Remap the mounted folder with the [bindfs command](https://bindfs.org/): `sudo bindfs --map=99/48:@99/@48 /mnt/LocalApertureImages /mnt/RetrofocusImages`
 - `48` and `@48` are the user and group ids of the **apache** Linux user (check this with `id apache`)
 - `99` and `@99` are the user and group ids of the `nobody` default HFS user (check this with `id nobody`)
 - run docker with the `-v` parameter: `docker run --rm  -v /mnt/RetrofocusImages:/var/www/html/Retrofocus/Aperture -p 8080:80 retrofocus`

### Running the Retrofocus application

1. If you build (`docker build [...]`) and start (`docker run [...]`) the docker application, then point your web browser to the [http://localhost:8080/Retrofocus](http://localhost:8080/Retrofocus) URL

2. If you use a CodeIgniter existing framework, then point your web browser to your Retrofocus application `http://my.host.com/my_web_app/Retrofocus` URL

Depending on your photo collection size, the Home window should come up in few seconds...

### Home window : the Library view

If the photo collection is correctly connected, Retrofocus first shows the "Library" view.

![The Library view](./screenshots/library.png)

The Library view is split up in 2 columns:
- the left "PROJECTS" column is the list of all Aperture projects (alphabetically ordered)
- the right "PROJECTS" column is the gallery of all Aperture projects with their corresponding "poster" (or "key" image)

You may either click on an item in the left list or in the gallery to open the corresponding "Project" view.

### The Project view

![The Project view](./screenshots/folder.png)

The Project view is split up in 2 columns:
- the left "PROJECTS" column is the list of all Aperture projects (alphabetically ordered), same as in the "Library" view
- the right column is the gallery of images in the selected project. Each displayed image in this view is called a "Version" image

> Aperture images may be of two kinds : "Master" or "Version". "Master" are the original files imported in the project, while "Version" are user editable samples. "Master" and "Version" should not be confused with "Raw" and "non Raw" images. "Master" and "Version" are parts of Aperture internal modifications management system while "Raw" and "non Raw" are image formats. Therefore, there are "Master" AND "Version" samples for both "Raw" AND "non Raw" images. "Version" images are modified with Aperture tools in a non destructive manner: the modifications are stored as a list of actions and applyied each time a "Version" needs to be displayed. Unfortunately Retrofocus can not replay these actions because it's basically only a "dumb" viewer: Retrofocus can only display already stored images...

If Aperture has generated a "Preview" for the "Version" displayed in the "Project", this preview is shown in the gallery, otherwise the `/!\` logo image is shown instead.

> V1.1 modification:

Below each "Version" thumbnail is displayed:
- the version number (`#0`, `#1`, `#2`, ...)
- the name or the title of the "Version"
- a `[V:XXXX]` button to open the "Version" detailed view. An optional `!` symbol means that a full size preview is available for this "Version". An optional `+` symbol means that some additional {"info"} properties are available for this version
- one or two `[M:XXXX]` buttons to open the available "Master" detailed views (a "Version" sample may be linked to one or two "Master" samples)
- the caption of the "Version"

Shortcuts in the **Project title bar**:
- click on the "PROJECTS" link to go back to the "Library" view

### The Master view

![The Master view](./screenshots/master.png)

The Master view is split up in 2 columns:
- the left "Master exiftool Info" column is the list of the image properties retrieved by `exiftool` for the Master original file
- the right column is a rendering of the "Master" sample. For most "non RAW" "Master" the rendering is available in JPEG format and may be displayed in the user browser. If no rendering is available the `/!\` image is shown instead.

Shortcuts in the **Master title bar**:
- click on the "PROJECTS" link  to go back to the "Library" view
- click on the project title link to go back to the parent "Project" view
- click on the `{"info"}` button to toggle display of the binary and file properties

### The Version view

![The Version view](./screenshots/version.png)

The Version view is split up in 2 columns:
- the left "Version sqlite Info" column is the list of the properties stored in the Aperture SQlite database for this "Version"
- the right column is a rendering of the "Version" sample. If no rendering is available the `/!\` image is shown instead (as shown in the example)

Shortcuts in the **version title bar**:
- click on the "PROJECTS" link to go back to the "Library" view
- click on the project title link to go back to the parent "Project" view
- click on the `{"info"}` button to toggle display of the binary and file properties

### The `{"info"}` view

> V1.1 new feature

![The info view](./screenshots/info.png)

This view is available in both "Master" and "Version" views by clicking on the `{"info"}` toggle button in the title bar.

This view displays most of the binaries and file properties available for the current "Master" of "Version" sample. Binaries properties are retrieved from the Aperture SQLite database while file properties are retrieved from the file system in the `Aperture Library.aplibrary` folder. The binaries and file properties are rendered as [JSON](https://www.json.org/json-en.html) objects in the `{"Info"}` view : then you can copy/paste them in a different software for further transforamtions.

You may theoritically reused these properties to render from scratch any Aperture images where adjustments have been applied ;-)

> These properties are used by Aperture to render each "Version" when users make any image adjustments (color, raw enhancements, white balance, red eyes correction, ...). It looks like Aperture is storing properties in both SQLite database and in the file system, then it's normal to find duplicates properties in the "Info" view...

## Credits

- Retrofocus is the name of a lens internal conception designed by [P. Angénieux](https://en.wikipedia.org/wiki/Pierre_Ang%c3%a9nieux) in 1950
- The Aperture software and the `/!\` image are copyrighted by the [Apple company](https://www.apple.com)
- The [CFPropertyList PHP package](https://github.com/TECLIB/CFPropertyList) from TECLIB is used in the decoding process of the Aperture properties
- The [Exiftool command line application](https://exiftool.org/) by Phil Harvey is used for retrieving EXIF data from image files
- The Retrofocus application is built upon the [CodeIgniter](https://codeigniter.com/) PHP framework

## Link and Author

https://gitlab.com/eric.yape/retrofocus

eric.yape@gmail.com
