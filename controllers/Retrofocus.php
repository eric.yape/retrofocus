<?php
/**
 * Retrofocus V1.1
 * Copyright (C) 2020 eric.yape@gmail.com
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ------------------------------------------------------------------------------
 * @author    eric.yape@gmail.com
 * @license   GNU GENERAL PUBLIC LICENSE Version 3
 * @link      https://gitlab.com/eric.yape/retrofocus
 */

class Retrofocus extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('Retrofocus_model');
                $this->load->helper('url_helper');

                $this->load->library('CFPropertyManager');

                date_default_timezone_set(RF_TIMEZONE);
        }

        public function index()
        {
                /* echo "Retrofocus::index()<br>"; */
                $this->library();
        }

        public function afac()
        {
                echo "Retrofocus::afac<br>";
        }

        public function library()
        {
                $data['folders'] = $this->Retrofocus_model->get_LibraryData();
                $data['title'] = 'Library';
        
                $this->load->view('templates/RF_header', $data);
                $this->load->view('Retrofocus/library', $data);
                $this->load->view('templates/RF_footer');
        }

        public function folder($token = NULL)
        {
                if (is_null($token)) {
                        return $this->library();
                }

                $data = $this->Retrofocus_model->get_FolderInfo($token);

                if (empty($data))
                {
                        show_404();
                }
                
                $data['versions'] = $this->Retrofocus_model->get_FolderData($token);
                $data['folders'] = $this->Retrofocus_model->get_LibraryData();
        
                $this->load->view('templates/RF_header', $data);
                $this->load->view('Retrofocus/folder', $data);
                $this->load->view('templates/RF_footer');
        }

        public function master($token = NULL)
        {
                if (is_null($token)) {
                        return $this->library();
                }

                $data['masterData'] = $this->Retrofocus_model->get_MasterData($token);

                if (empty($data['masterData']))
                {
                        show_404();
                }

                $data['title'] = $data['masterData']['Meta.name'];
        
                $this->load->view('templates/RF_header', $data);
                $this->load->view('Retrofocus/master', $data);
                $this->load->view('templates/RF_footer');
        }

        public function version($token = NULL)
        {
                if (is_null($token)) {
                        return $this->library();
                }

                $data['versionData'] = $this->Retrofocus_model->get_VersionData($token);

                if (empty($data['versionData']))
                {
                        show_404();
                }

                $data['title'] = $data['versionData']['Version.name'];
        
                $this->load->view('templates/RF_header', $data);
                $this->load->view('Retrofocus/version', $data);
                $this->load->view('templates/RF_footer');
        }
}
?>