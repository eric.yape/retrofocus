function changeClass(elementId, className) {
    document.getElementById(elementId).className = className; 
}

function displayStyles(elementId) {
    var element = document.getElementById(elementId);
    var styles = window.getComputedStyle(element, null);

    var len = styles.length;
    for (var i=0; i<len; i++) {
   
      var style = styles[i];
      console.log(style + " : " + styles.getPropertyValue(style));
    }

}

function getProperty(elementId, prop) {
    var element = document.getElementById(elementId);
    var styles = window.getComputedStyle(element, null);
    return parseInt( styles.getPropertyValue(prop) );
}

function getHeight(elementId) {
    return getProperty(elementId, "height");
}

function getWidth(elementId) {
    return getProperty(elementId, "width");
}

function getBorderHeight(elementId) {

    var element = document.getElementById(elementId);

    if (element != null) {
        var styles = window.getComputedStyle(element, null);

        var padding = parseInt( styles.getPropertyValue("padding-top") ) + parseInt( styles.getPropertyValue("padding-bottom") );
        var border = parseInt( styles.getPropertyValue("border-top-width") ) + parseInt( styles.getPropertyValue("border-bottom-width") );
        var margin = parseInt( styles.getPropertyValue("margin-top") ) + parseInt( styles.getPropertyValue("margin-bottom") );

        return  padding + border + margin;
    }
    else return 0;
}

function getBorderWidth(elementId) {

    var element = document.getElementById(elementId);

    if (element != null) {
        var styles = window.getComputedStyle(element, null);

        var padding = parseInt( styles.getPropertyValue("padding-left") ) + parseInt( styles.getPropertyValue("padding-right") );
        var border = parseInt( styles.getPropertyValue("border-left-width") ) + parseInt( styles.getPropertyValue("border-right-width") );
        var margin = parseInt( styles.getPropertyValue("margin-left") ) + parseInt( styles.getPropertyValue("margin-right") );

        return  padding + border + margin;
    }
    else return 0;
}

function getOuterHeight(elementId) {

    return getHeight(elementId) + getBorderHeight(elementId);
}

function getOuterWidth(elementId) {

    return getWidth(elementId) + getBorderWidth(elementId);
}

function displayOuterHeight(elementId) {

    var outerHeight = getOuterHeight(elementId);

    console.log(`outerHeight ${elementId} = ${outerHeight}`);
}

function displayBorderHeight(elementId) {

    var borderHeight = getBorderHeight(elementId);

    console.log(`borderHeight ${elementId} = ${borderHeight}`);
}

function setHeight(elementId, pixelHeight) {

    var element = document.getElementById(elementId);
    if (element != null) {
        element.style.height = `${pixelHeight}px`;
    }
}

function setWidth(elementId, pixelWidth) {
    var element = document.getElementById(elementId);
    if (element != null) {
        element.style.width = `${pixelWidth}px`; 
    }
}

function setViewersDimension() {
    setHeight("#index", getHeight("#column1") - getOuterHeight("#title1") - getBorderHeight("#index"));

    setHeight("#gallery", getHeight("#column2") - getOuterHeight("#title2") - getBorderHeight("#gallery"));
    setWidth("#gallery", getWidth("#column2") - getBorderWidth("#gallery"));
    
    setHeight("#raw_viewer", getHeight("#column2") - getOuterHeight("#title2") - getBorderHeight("#raw_viewer"));
    setWidth("#raw_viewer", getWidth("#column2") - getBorderWidth("#raw_viewer"));
 }

 function scrollToIndex(elementId) {
    var myElement = document.getElementById(elementId);
    var topPos = myElement.offsetTop;
    console.log(`topPos ${elementId} = ${topPos}`);
    document.getElementById("#index").scrollTop = topPos - 32;
 }

 function version_raw() {

    var gallery = document.getElementById("#gallery");
    var raw_viewer = document.getElementById("#raw_viewer");
    if (gallery.style.display === "none") {
        gallery.style.display = "block";
        raw_viewer.style.display = "none";
      } else {
        gallery.style.display = "none";
        raw_viewer.style.display = "block";
      }
 }