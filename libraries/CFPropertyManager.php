<?php
/**
 * Retrofocus V1.1
 * Copyright (C) 2020 eric.yape@gmail.com
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ------------------------------------------------------------------------------
 * @author    eric.yape@gmail.com
 * @license   GNU GENERAL PUBLIC LICENSE Version 3
 * @link      https://gitlab.com/eric.yape/retrofocus
 */

defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . '/libraries/CFPropertyList/CFType.php');
require_once(APPPATH . '/libraries/CFPropertyList/CFArray.php');
require_once(APPPATH . '/libraries/CFPropertyList/CFBinaryPropertyList.php');
require_once(APPPATH . '/libraries/CFPropertyList/CFBoolean.php');
require_once(APPPATH . '/libraries/CFPropertyList/CFData.php');
require_once(APPPATH . '/libraries/CFPropertyList/CFDate.php');
require_once(APPPATH . '/libraries/CFPropertyList/CFDictionary.php');
require_once(APPPATH . '/libraries/CFPropertyList/CFNumber.php');
require_once(APPPATH . '/libraries/CFPropertyList/CFPropertyList.php');
require_once(APPPATH . '/libraries/CFPropertyList/CFString.php');
require_once(APPPATH . '/libraries/CFPropertyList/CFTypeDetector.php');
require_once(APPPATH . '/libraries/CFPropertyList/CFUid.php');
require_once(APPPATH . '/libraries/CFPropertyList/IOException.php');
require_once(APPPATH . '/libraries/CFPropertyList/PListException.php');

define('HINT_NONE', 0);
define('HINT_DATE', 1);

class CFPropertyManager {

    private $props = null;
    private $tab = "\n";

    private static $searchA = array('"', "\n");
    private static $replaceA = array('\\"', "\\n");

    private $accessedObjects = array();

    private function getProp($object) {

        if (!is_int($object)) {
            /* Oups : wrong object accessed ! */
            throw new Exception("wrong object accessed near {$object}");
        }

        if (array_key_exists($object, $this->accessedObjects)) {

            $n = $this->accessedObjects[$object];

            if ($n > 100) {
                /* Oups : infinite loop detected !! */
                throw new Exception("Infinite loop detected near {$object}");
            }
            else
                $this->accessedObjects[$object] = $n++;
        }
        else
            $this->accessedObjects[$object] = 0;

        return $this->props[$object];
    }

    public function setTab($t) {
        $this->tab = $t;
    }

    function isDate($key) {

        return !strcmp(substr($key, -4), "Date");
    }

    function format_date($object) {

        if (!is_float($object) && !is_int($object))
            return "";

        if (is_float($object))
            $object = intval($object);

        $dt = new DateTime("@$object");
        $fmt = $dt->format('Y-m-d H:i:s');

        //echo "$epoch = $fmt\n";
        return " ({$fmt})";
    }

    function format_string($s) {

        return str_replace(self::$searchA, self::$replaceA, strval($s));
    }

    function format_KeyValue($key, $value) {

        return $this->isDate($key) ? 
            ($this->tab . '"' . $key . '": ' . $this->format_object($value, HINT_DATE))
            :
            ($this->tab . '"' . $key . '": ' . $this->format_object($value));
    }

    function format_object($object, $hint = HINT_NONE) {

        global $dbg;
        
        if ($dbg) {
            echo "\nobject = \n";
            print_r($object);

            if ($hint == HINT_DATE) {
                echo "date ? $object\n";
            }
        }

        $fmt = "?";

        if (is_array($object)) {

            $this->tab .= "    ";

            if (array_key_exists('$class', $object)) {

                $class = $object['$class'];
                $classDef = $this->props[$class];
                $className = $classDef['$classname'];
                //$classClasses = $classDef['$classes'];
                if ($dbg) echo "\nclassName = $className\n";

                /*
                * Hard-coded classes
                * Cf comment below...
                */
                if (!strcmp($className, "NSDictionary") || !strcmp($className, "NSMutableDictionary")) {
                    $fmt = "{";
                    $ns_keys = $object['NS.keys'];
                    $ns_objects = $object['NS.objects'];

                    for ($i = 0; $i < count($ns_objects); $i++) {
                        if ($i) {
                            $fmt .= ",";
                        }
                        $fmt .= $this->format_KeyValue($this->getProp($ns_keys[$i]), $this->getProp($ns_objects[$i]));
                    }
                    $fmt .= "}";
                }

                else if (!strcmp($className, "NSArray") || !strcmp($className, "NSMutableArray")) {
                    $fmt = "[";
                    $ns_objects = $object['NS.objects'];
                    for ($i = 0; $i < count($ns_objects); $i++) {
                        if ($i) {
                            $fmt .= ",";
                        }
                        $fmt .= $this->tab . $this->format_object($this->getProp($ns_objects[$i]));
                    }
                    $fmt .= "]";
                }

                else if (!strcmp($className, "NSNull")) {
                    $fmt = "null";
                }

                else if (!strcmp($className, "NSMutableData")) {
                    $data = $object['NS.data'];
                    /* #1
                    $data = preg_replace('/[^ -~]/', ".", $data);
                    $fmt = '"'. $this->format_string($data) . '"'; */
                    /* #2
                    $fmt = '"'. bin2hex($data) . '"'; */
                    $fmt = '"'. base64_encode($data) . '"';
                }

                else if (!strcmp($className, "NSColor") || !strcmp($className, "DGControlPoint")) {
                    $fmt = "{";
                    $i = 0;
                    foreach ($object as $key => $value) {
                        if (strcmp($key, '$class')) {
                            if ($i) {
                                $fmt .= ",";
                            }
                            $fmt .= $this->format_KeyValue($key, $value);
                            $i++;
                        }
                    }
                    $fmt .= "}";
                }

                else if (!strcmp($className, "RKRetouchBrushStroke") || !strcmp($className, "RKMutableRetouchBrushStroke")) {
                    $fmt = "{";
                    $i = 0;
                    foreach ($object as $key => $value) {
                        if (strcmp($key, '$class')) {
                            if ($i) {
                                $fmt .= ",";
                            }
                            if (!strcmp($key, "sourceOffset") || !strcmp($key, "brushStroke")) {
                                $fmt .= $this->format_KeyValue($key, $this->getProp($value));
                            }
                            else {
                                $fmt .= $this->format_KeyValue($key, $value);
                            }
                            $i++;
                        }
                    }
                    $fmt .= "}";
                }

                else if (!strcmp($className, "DGBrushStroke") || !strcmp($className, "DGMutableBrushStroke")) {
                    $fmt = "{";
                    $i = 0;
                    foreach ($object as $key => $value) {
                        if (strcmp($key, '$class')) {
                            if ($i) {
                                $fmt .= ",";
                            }
                            if (!strcmp($key, "data")) {
                                $fmt .= $this->format_KeyValue($key, $this->getProp($value));
                            }
                            else {
                                $fmt .= $this->format_KeyValue($key, $value);
                            }
                            $i++;
                        }
                    }
                    $fmt .= "}";
                }

                else if (!strcmp($className, "DGRedEyeSpot")) {
                    $fmt = "{";
                    $i = 0;
                    foreach ($object as $key => $value) {
                        if (strcmp($key, '$class')) {
                            if ($i) {
                                $fmt .= ",";
                            }
                            if (!strcmp($key, "DGRedEyeSpot_center")) {
                                $fmt .= $this->format_KeyValue($key, $this->getProp($value));
                            }
                            else {
                                $fmt .= $this->format_KeyValue($key, $value);
                            }
                            $i++;
                        }
                    }
                    $fmt .= "}";
                }

                else if (!strcmp($className, "DGSpotPatchSpot") || !strcmp($className, "DGMutableSpotPatchSpot")) {
                    $fmt = "{";
                    $i = 0;
                    foreach ($object as $key => $value) {
                        if (strcmp($key, '$class')) {
                            if ($i) {
                                $fmt .= ",";
                            }
                            if (!strcmp($key, "DGSpotPatchSpot_center") || !strcmp($key, "DGSpotPatchSpot_patchCenter")) {
                                $fmt .= $this->format_KeyValue($key, $this->getProp($value));
                            }
                            else {
                                $fmt .= $this->format_KeyValue($key, $value);
                            }
                            $i++;
                        }
                    }
                    $fmt .= "}";
                }

                /*
                * If the real '$object' class name is not listed in the previous hard-coded list (NSColor, DGControlPoint,
                * RKRetouchBrushStroke, ...) then there is a possibility that this object could not be correctly decoded
                * by the following 'foreach' statement if one of its attributes is not an object...
                * However, the CFPropertyManager class should safely handle this misconception (!) and throws a PHP exception
                */
                else {
                    $fmt = "{ \"$className\": {";
                    $i = 0;
                    foreach ($object as $key => $value) {
                        if (strcmp($key, '$class')) {
                            if ($i) {
                                $fmt .= ",";
                            }
                            $fmt .= $this->format_KeyValue($key, $this->getProp($value));
                            $i++;
                        }
                    }
                    $fmt .= "} }";
                }
                /*
                * Phew ;-)
                */
            }
            else {
                // anonymous array
                $keys = array_keys($object);
                if (count($keys) > 0) {
                    $fmt = "{";
                    $i = 0;
                    foreach ($object as $key => $value) {
                        if ($i) {
                            $fmt .= ",";
                        }
                        $fmt .= $this->format_KeyValue($key, $value);
                        $i++;
                    }
                    $fmt .= "}";
                }
                else {
                    $fmt = "[";
                    $i = 0;
                    foreach ($object as $item) {
                        if ($i) {
                            $fmt .= ",";
                        }
                        $fmt .= $this->tab . $this->format_object($item);
                        $i++;
                    }
                    $fmt .= "]";
                }
            }

            $this->tab = substr($this->tab, 0, -4);

        }
        else {
            if (is_string($object) && !strcmp(substr($object, 0, 8), "bplist00")) {
                /* binary property ! */
                $nestedPropMan = new CFPropertyManager();
                $fmt = $nestedPropMan->format_binary($object, $this->tab);
            }
            else if (is_string($object) && !strcmp(substr($object, 2, 11), "streamtyped")) {
                /* streamtyped property ? */
                $fmt = '"'. base64_encode($object) . '"';
            }
            else if ($hint == HINT_DATE) {
                /* date ? */
                $fmt = '"'. $this->format_string($object) . $this->format_date($object) . '"';
            }
            else {
                // scalar value (converted to string)
                $fmt = '"' . $this->format_string($object) . '"';
            }
        }

        //echo "\nfmt = $fmt\n";
        return $fmt;
    }

    function format_binary($binary, $tab = "\n") {

        if (is_null($binary))
            return "null";

        $plist = new CFPropertyList\CFPropertyList();
        $plist->parseBinary($binary);

        $this->props = $plist->toArray()['$objects'];
        $this->tab = $tab;
        //print_r($plist->toArray());

        try {
            return $this->format_object($this->getProp(1));
        }
        catch (Exception $e) {
            return "\"CFPropertyManager:: catch error:'" . $e->getMessage() . "' !\"";
        }
    }

    function format_file($filename, $tab = "\n") {

        if (!is_readable($filename))
            return "\"CFPropertyManager:: file '" . $filename . "' can not be read !\"";
    
        $plist = new CFPropertyList\CFPropertyList( $filename, CFPropertyList\CFPropertyList::FORMAT_BINARY );

        $this->props = $plist->toArray();
        $this->tab = $tab;
        //print_r( $this->props );

        try {
            return $this->format_object($this->props);
        }
        catch (Exception $e) {
            return "\"CFPropertyManager:: catch error:'" . $e->getMessage() . "' !\"";
        }
    }
}