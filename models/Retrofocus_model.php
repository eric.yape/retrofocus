<?php
/**
 * Retrofocus V1.1
 * Copyright (C) 2020 eric.yape@gmail.com
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ------------------------------------------------------------------------------
 * @author    eric.yape@gmail.com
 * @license   GNU GENERAL PUBLIC LICENSE Version 3
 * @link      https://gitlab.com/eric.yape/retrofocus
 */

class Retrofocus_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
                $this->db->query("attach database '" . RF_LIBRARY_PATH . "/Database/apdb/ImageProxies.apdb' as ImageProxies");
                $this->db->query("attach database '" . RF_LIBRARY_PATH . "/Database/apdb/Properties.apdb' as Properties");
                $this->db->query("attach database '" . RF_LIBRARY_PATH . "/Database/apdb/BigBlobs.apdb' as BigBlobs");
        }

        public function get_LibraryData()
        {
                $sql = <<< SQL_LibraryData
                SELECT  RKFolder.modelId,
                        RKFolder.uuid,
                        RKFolder.name,
                        RKImageProxyState.miniThumbnailPath
                FROM    RKFolder LEFT JOIN ImageProxies.RKImageProxyState ON (RKFolder.posterVersionUuid = RKImageProxyState.versionUuid)
                WHERE   parentFolderUuid='AllProjectsItem' AND isInTrash=0 AND folderType=2
                ORDER   BY 3 ASC 
SQL_LibraryData;
                $query = $this->db->query($sql);
                return $query->result_array();
        }

        public function get_FolderInfo($RKFolder_modelId)
        {
                $query = $this->db->query("select modelId, name from RKFolder where modelId=$RKFolder_modelId");
                return $query->row_array();
        }

        public function get_FolderData($RKFolder_modelId = FALSE)
        {
                if ($RKFolder_modelId === FALSE)
                {
                        return Array();
                }

                $sql = <<< SQL_FolderData
       SELECT   RKVersion.name,
                RKVersion.versionNumber,
                RKVersion.modelId              AS versionModelId,
                RKVersion.fileName             AS versionFileName,

                RawMaster.modelId              AS rawMasterModelId,
                RawMaster.filename             AS rawMasterFileName,
                RawMaster.imagePath            AS rawMasterImagePath,
                NonRawMaster.modelId           AS nonRawMasterModelId,
                NonRawMaster.filename          AS nonRawMasterFileName,
                NonRawMaster.imagePath         AS nonRawMasterImagePath,

                RKImageProxyState.fullsizepreviewpath,
                RKImageProxyState.thumbnailpath,
                RKImageProxyState.minithumbnailpath,
                RKImageProxyState.minithumbnailwidth,
                RKImageProxyState.minithumbnailheight,

                RKUniqueString.stringproperty AS caption,
                CASE
                  WHEN cgImageData.uuid IS NULL THEN NULL
                  ELSE 1
                END                           AS cgImageDataF
         FROM   RKFolder,
                RKVersion
                LEFT JOIN RKMaster RawMaster
                       ON ( RKVersion.rawMasterUuid = RawMaster.uuid )
                LEFT JOIN RKMaster NonRawMaster
                       ON ( RKVersion.nonRawMasterUuid = NonRawMaster.uuid )
                LEFT JOIN ImageProxies.RKImageProxyState
                       ON ( RKVersion.uuid = RKImageProxyState.versionuuid )
                LEFT JOIN BigBlobs.cgImageData
                       ON ( RKVersion.uuid = cgImageData.uuid )
                LEFT JOIN Properties.RKIptcProperty
                       ON ( RKVersion.modelid = RKIptcProperty.versionid
                            AND RKIptcProperty.propertykey = 'Caption/Abstract' )
                LEFT JOIN Properties.RKUniqueString
                       ON ( RKIptcProperty.stringid = RKUniqueString.modelid )
         WHERE  RKVersion.projectUuid = RKFolder.uuid
                AND RKFolder.modelId = $RKFolder_modelId
         ORDER  BY 1, 2, 3 ASC
SQL_FolderData;

                $query = $this->db->query($sql);
                return $query->result_array();
        }

        public function get_MasterData($RKMaster_modelId = FALSE)
        {
                if ($RKMaster_modelId === FALSE)
                {
                        return null;
                }

                $sql = <<< SQL_MasterData
        SELECT  RKMaster.name        AS "Meta.name",
                RKMaster.imagePath   AS "Meta.imagePath",
                RKMaster.subtype     AS "Meta.subtype",
                RKMaster.projectUuid AS "Meta.projectUuid",
                RKMaster.uuid        AS "Meta.uuid",
                RKMaster.fileName    AS "Meta.fileName",
                RKFolder.name        AS "Meta.projectName",
                RKFolder.modelId     AS "Meta.projectModelId",
                strftime("%d/%m/%Y %H:%M:%S", datetime(RKMaster.imageDate + 978307200, 'unixepoch', 'localtime')) AS "Master.imageDate"

         FROM   RKMaster,
                RKFolder
         WHERE  RKMaster.projectUuid = RKFolder.uuid
                AND RKMaster.modelId = $RKMaster_modelId 
SQL_MasterData;

                $data = $this->db->query($sql)->row_array();

                $imageFile = RF_LIBRARY_PATH . "/Masters/" . $data['Meta.imagePath'];
                exec("exiftool -S -sort \"$imageFile\"", $output);

                foreach($output as $exifdata) {
                        $p = strpos($exifdata, ":");
                        $key = substr($exifdata, 0, $p);
                        $value = substr($exifdata, $p+1);
                        $data[$key] = $value;
                }

                $data['Meta.imageFile'] = RF_LIBRARY_FOLDER . "/Masters/" . $data['Meta.imagePath'];
                $data['Meta.Model'] = $this->get_Tag($data, array("Model"));
                $data['Meta.LensModel'] = $this->get_Tag($data, array("LensModel", "LensID"));
                $data['Meta.ISO'] = $this->get_Tag($data, array("ISO"));
                $data['Meta.Aperture'] = $this->get_Tag($data, array("Aperture"));
                $data['Meta.Speed'] = $this->get_Tag($data, array("ShutterSpeed"));
                $data['Meta.FocalLength'] = $this->get_Tag($data, array("FocalLength"));
                $data['Meta.EV'] = $this->get_Tag($data, array("ExposureShift","AEBXv","ExposureCompensation"));

                $master_dirname = RF_LIBRARY_PATH . '/Database/Versions';
                $master_folder = substr($data['Meta.imagePath'], 0, - strlen($data['Meta.fileName']) - 1);
                $master_filePath = "$master_dirname/$master_folder/{$data['Meta.uuid']}/Master.apmaster";
                $master_fileData = "\"$master_filePath\": " . $this->cfpropertymanager->format_file($master_filePath);
  
                $data["Meta.BinaryData"] = $master_fileData;

                return $data;
        }

        public function get_Tag($exifData, $tags)
        {
                foreach($tags as $tag) {
                        if (array_key_exists($tag, $exifData)) {
                                return $exifData[$tag];
                        }
                }
                return "?";
        }

        public function get_VersionData($RKVersion_modelId = FALSE)
        {
              if ($RKVersion_modelId === FALSE)
              {
                     return null;
              }

              $sql = <<< SQL_VersionData
        SELECT  RKVersion.name                        AS "Version.name",
                RKVersion.fileName                    AS "Version.fileName",
                RKVersion.fileName                    AS "Meta.fileName",
                RKVersion.versionNumber               AS "Version.versionNumber",
                RKImageProxyState.fullSizePreviewPath AS "Meta.fullSizePreviewPath",
                RKFolder.name                         AS "Meta.projectName",
                RKFolder.modelId                      AS "Meta.projectModelId",
                RKVersion.mainRating                  AS "Version.mainRating",
                RKVersion.masterHeight                AS "Version.masterHeight",
                RKVersion.masterWidth                 AS "Version.masterWidth",
                strftime("%d/%m/%Y %H:%M:%S", datetime(RKVersion.imageDate + 978307200, 'unixepoch', 'localtime')) AS "Version.imageDate",
                cgImageData.cgImageData               AS "Meta.cgImageData",
                RKMaster.imagePath                    AS "Meta.masterImagePath",
                RKMaster.fileName                     AS "Meta.masterFileName",
                RKMaster.uuid                         AS "Meta.masterUuid",
                RKImageAdjustment.name                AS "Meta.imageAdjustmentName",
                RKImageAdjustment.data                AS "Meta.imageAdjustmentData"

         FROM   RKVersion
                LEFT JOIN ImageProxies.RKImageProxyState
                       ON ( RKVersion.uuid = RKImageProxyState.versionUuid )
                LEFT JOIN BigBlobs.cgImageData
                       ON ( RKVersion.uuid = cgImageData.uuid )
                LEFT JOIN RKImageAdjustment
                       ON ( RKVersion.uuid = RKImageAdjustment.versionUuid ),
                RKFolder,
                RKMaster
         WHERE  RKFolder.uuid = RKVersion.projectUuid
                AND RKMaster.uuid = RKVersion.masterUuid
                AND RKVersion.modelId = $RKVersion_modelId
SQL_VersionData;

              $versionData = $this->db->query($sql)->row_array();

              $sql_Adjustments = <<< SQL_ADJ
        SELECT  RKImageAdjustment.adjIndex,
                RKImageAdjustment.data,
                RKImageAdjustment.dbVersion,
                RKImageAdjustment.isEnabled,
                RKImageAdjustment.maskUuid,
                RKImageAdjustment.modelId,
                RKImageAdjustment.name,
                RKImageAdjustment.uuid,
                RKImageAdjustment.versionUuid
         FROM   RKVersion
                LEFT JOIN RKImageAdjustment
                       ON ( RKVersion.uuid = RKImageAdjustment.versionUuid )
         WHERE  RKImageAdjustment.name IS NOT NULL AND RKVersion.modelId = $RKVersion_modelId
         ORDER BY 1 ASC
SQL_ADJ;
              $adjData = $this->db->query($sql_Adjustments)->result_array();

              $version_ImageAdjustments = "\"Library.RKImageAdjustment.data\": {";
              $i = 0;
              foreach ($adjData as $adj) {
                     if ($i) {
                            $version_ImageAdjustments .= ",";
                     }
                     $tab = "\n    ";
                     $version_ImageAdjustments .= "$tab\"$i\": {";
                     $tab .= "    ";
                     $version_ImageAdjustments .= "$tab\"name\": \"{$adj["name"]}\",";
                     $version_ImageAdjustments .= "$tab\"data\": {$this->cfpropertymanager->format_binary($adj["data"], $tab)},";
                     $version_ImageAdjustments .= "$tab\"adjIndex\": \"{$adj["adjIndex"]}\",";
                     $version_ImageAdjustments .= "$tab\"dbVersion\": \"{$adj["dbVersion"]}\",";
                     $version_ImageAdjustments .= "$tab\"isEnabled\": \"{$adj["isEnabled"]}\",";
                     $version_ImageAdjustments .= "$tab\"maskUuid\": \"{$adj["maskUuid"]}\",";
                     $version_ImageAdjustments .= "$tab\"modelId\": \"{$adj["modelId"]}\",";
                     $version_ImageAdjustments .= "$tab\"uuid\": \"{$adj["uuid"]}\",";
                     $version_ImageAdjustments .= "$tab\"versionUuid\": \"{$adj["versionUuid"]}\"";
                     $version_ImageAdjustments .= "}";
                     $i++;
              }
              $version_ImageAdjustments .= "}";

              $sql_Props = <<< SQL_PROPS
        SELECT  key,
                value
         FROM   (SELECT 'Iptc.'
                        || RKIptcProperty.propertykey AS key,
                        RKUniqueString.stringproperty AS value
                 FROM   RKVersion
                        LEFT JOIN properties.RKIptcProperty
                               ON ( RKVersion.modelid = RKIptcProperty.versionid )
                        LEFT JOIN properties.RKUniqueString
                               ON ( RKIptcProperty.stringid = RKUniqueString.modelid )
                 WHERE  RKVersion.modelId = $RKVersion_modelId
                 UNION
                 SELECT 'Exif.'
                        || RKExifStringProperty.propertykey,
                        RKUniqueString.stringproperty
                 FROM   RKVersion
                        LEFT JOIN properties.RKExifStringProperty
                               ON ( RKVersion.modelid = RKExifStringProperty.versionid )
                        LEFT JOIN properties.RKUniqueString
                               ON ( RKExifStringProperty.stringid =
                                    RKUniqueString.modelid )
                 WHERE  RKVersion.modelId = $RKVersion_modelId
                 UNION
                 SELECT 'Exif.'
                        || RKExifNumberProperty.propertykey,
                        ''
                        || RKExifNumberProperty.numberproperty
                 FROM   RKVersion
                        LEFT JOIN properties.RKExifNumberProperty
                               ON ( RKVersion.modelid = RKExifNumberProperty.versionid )
                 WHERE  RKVersion.modelId = $RKVersion_modelId
                 UNION
                 SELECT 'Other.'
                        || RKOtherProperty.propertykey,
                        RKUniqueString.stringproperty
                 FROM   RKVersion
                        LEFT JOIN properties.RKOtherProperty
                               ON ( RKVersion.modelid = RKOtherProperty.versionid )
                        LEFT JOIN properties.RKUniqueString
                               ON ( RKOtherProperty.stringid = RKUniqueString.modelid )
                 WHERE  RKVersion.modelId = $RKVersion_modelId)
         WHERE  key IS NOT NULL
         ORDER  BY 1 ASC
SQL_PROPS;

              $propsData = $this->db->query($sql_Props)->result_array();
              foreach ($propsData as $prop) {
                     $versionData[$prop['key']] = $prop['value'];
              }

              $versionData['Meta.Model'] = $this->get_Tag($versionData, array("Exif.Model"));
              $versionData['Meta.LensModel'] =  $this->get_Tag($versionData, array("Exif.LensModel"));
              $versionData['Meta.ISO'] = $this->get_Tag($versionData, array("Exif.ISOSpeedRating"));
              $versionData['Meta.Aperture'] = $this->get_Tag($versionData, array("Exif.ApertureValue"));

              $f = floatval($this->get_Tag($versionData, array("Exif.ShutterSpeed")));
              $versionData['Meta.Speed'] = ($f != 0) ? ("1/" . strval(round(1.0 / $f))) : "?";

              $versionData['Meta.FocalLength'] = $this->get_Tag($versionData, array("Exif.FocalLength"))." mm";
              $versionData['Meta.EV'] = $this->get_Tag($versionData, array("Exif.ExposureBiasValue"));

              $version_cgImageData = "\"BigBlobs.cgImageData.cgImageData\": "
                     . ((is_null($versionData["Meta.cgImageData"])) ?
                            "null"
                            :
                            $this->cfpropertymanager->format_binary($versionData["Meta.cgImageData"]));

              $version_dirname = RF_LIBRARY_PATH . '/Database/Versions';
              $version_folder = substr($versionData['Meta.masterImagePath'], 0, - strlen($versionData['Meta.masterFileName']) - 1);
              $version_filePath = "$version_dirname/$version_folder/{$versionData['Meta.masterUuid']}/Version-{$versionData['Version.versionNumber']}.apversion";
              //echo $version_filePath;
              $version_fileData = "\"$version_filePath\": " . $this->cfpropertymanager->format_file($version_filePath);

              $versionData["Meta.BinaryData"] = "{\n"
                     . $version_cgImageData . ",\n\n"
                     . $version_ImageAdjustments . ",\n\n"
                     . $version_fileData . "\n}";

              return $versionData;
        }
}
?>