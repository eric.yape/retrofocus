<!-- Retrofocus V1.1 Copyright (C) 2020 eric.yape@gmail.com -->
<div class="folder" id="#folder">
    <div class="column1" id="#column1">
        <div class="title" id="#title1">PROJECTS</div>
        <div class="index" id="#index">
            <?php foreach ($folders as $folder): ?>
                <?php $href = site_url('retrofocus/folder/'. urlencode($folder['modelId'])); ?>
                <div class="index_item" id="<?php echo $folder['modelId']?>"><a href="<?php echo $href; ?>"><?php echo $folder['name']; ?></a></div>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="column2" id="#column2">
        <div class="title" id="#title2"><a href="<?php echo site_url('retrofocus/library');?>">PROJECTS</a> / Project: <?php echo $name; ?></div>
        <div class="gallery" id="#gallery">
            <?php foreach ($versions as $item): ?>
                <?php
                        
                        $hrefVersionInfo = site_url('retrofocus/version/'. $item['versionModelId']);
                        $hrefRawMasterInfo = site_url('retrofocus/master/' . $item['rawMasterModelId']);
                        $hrefNonRawMasterInfo = site_url('retrofocus/master/' . $item['nonRawMasterModelId']);
                        $versionProp = "";

                        if (isset($item['fullSizePreviewPath']))
                        {
                            $versionHref = RF_LIBRARY_FOLDER . "/Previews/" . str_replace('%', '%25', $item['fullSizePreviewPath']);
                            $versionProp .= "!";
                        }
                        else if (isset($item['nonRawMasterImagePath']))
                        {
                            $versionHref = RF_LIBRARY_FOLDER . "/Masters/" . str_replace('%', '%25', $item["nonRawMasterImagePath"]);
                        }
                        else if (isset($item['rawMasterImagePath']))
                        {
                            $versionHref = RF_LIBRARY_FOLDER . "/Masters/" . str_replace('%', '%25', $item["rawMasterImagePath"]);
                        }

                        if (isset($item['miniThumbnailPath']))
                        {
                            $versionImg = RF_LIBRARY_FOLDER . "/Thumbnails/" . str_replace('%', '%25', $item['miniThumbnailPath']);
                            $versionWidth = $item['miniThumbnailWidth'];
                            $versionHeight = $item['miniThumbnailHeight'];
                        }
                        else
                        {
                            $versionImg = "/Retrofocus/images/UnsupportedImage_300x200.png";
                            $versionWidth = 300;
                            $versionHeight = 200;
                        }
                        if (isset($item['cgImageDataF'])) $versionProp .= "+";
                ?>
                <div class="version" style="width: <?php echo $versionWidth;?>">
                    <div class="version_content">
                        <a href="<?php echo $versionHref; ?>"><img src="<?php echo $versionImg;?>" width="<?php echo $versionWidth;?>" height="<?php echo $versionHeight;?>"></a>
                    </div>
                    <div class="version_footer" >
                        <?php echo "#{$item['versionNumber']} {$item['name']}"; ?>
                    </div>
                    <div class="version_footer" >
                        <div class="version_info"><a href="<?php echo $hrefVersionInfo;?>"><?php echo "V:" . $item['versionFileName'] . $versionProp;?></a></div><?php 
                            if (isset($item['nonRawMasterModelId']) && ($item['rawMasterModelId'] != $item['nonRawMasterModelId'])): ?><div class="version_info"><a href="<?php echo $hrefNonRawMasterInfo;?>"><?php echo "M:" . $item['nonRawMasterFileName']; ?></a></div><?php endif; ?><?php
                            if (isset($item['rawMasterModelId'])): ?><div class="version_info"><a href="<?php echo $hrefRawMasterInfo;?>"><?php echo "M:" . $item['rawMasterFileName']; ?></a></div><?php endif; ?>
                        </div>
                    <?php if (isset($item['caption'])): ?>
                        <div class="version_caption">
                                <?php echo $item['caption']; ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    setViewersDimension();
    window.onresize = setViewersDimension;

    changeClass("<?php echo $modelId;?>", "index_selected");
    scrollToIndex("<?php echo $modelId;?>");
</script>