<!-- Retrofocus V1.1 Copyright (C) 2020 eric.yape@gmail.com -->
<div class="folder" id="#folder">
    <div class="column1" id="#column1">
        <div class="title" id="#title1">PROJECTS</div>
        <div class="index" id="#index">
            <?php foreach ($folders as $folder): ?>
                <?php $href = site_url('retrofocus/folder/'. urlencode($folder['modelId'])); ?>
                <div class="index_item" id="<?php echo $folder['modelId']?>"><a href="<?php echo $href; ?>"><?php echo $folder['name']; ?></a></div>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="column2" id="#column2">
        <div class="title" id="#title2"><a href="<?php echo site_url('retrofocus/library');?>">PROJECTS</a></div>
        <div class="gallery" id="#gallery">
            <?php foreach ($folders as $folder): ?>
                <div class="version">
                    <?php
                            $hrefMini = RF_LIBRARY_FOLDER . "/Thumbnails/" . str_replace('%', '%25', $folder['miniThumbnailPath']);
                            $hrefFolderInfo = site_url('retrofocus/folder/'. urlencode($folder['modelId']));
                    ?>
                    <div class="folder_content">
                        <a href="<?php echo $hrefFolderInfo; ?>">
                            <img class="folder_poster"
                                src="<?php echo isset($folder['miniThumbnailPath']) ? $hrefMini : "/Retrofocus/images/UnsupportedImage_200x133.png";?>">
                        </a>
                    </div>
                    <div class="folder_footer">
                        <?php echo $folder['name']; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    setViewersDimension();
    window.onresize = setViewersDimension;
</script>