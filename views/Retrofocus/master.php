<!-- Retrofocus V1.1 Copyright (C) 2020 eric.yape@gmail.com -->
<div class="folder" id="#folder">
    <div class="column1" id="#column1">
        <div class="title" id="#title1">Master exiftool Info</div>
        <div class="index" id="#index">
            <div class="index_short">
                <div class="index_main"><?php echo $masterData['Meta.Model']; ?></div>
                <div class="index_main"><?php echo $masterData['Meta.LensModel']; ?></div>
                <table class="index_mini">
                <tr>
                    <td class="index_main">ISO <?php echo $masterData['Meta.ISO'];?></td>
                    <td class="index_main"> | <?php echo $masterData['Meta.FocalLength']; ?></td>
                    <td class="index_main"> | <?php echo $masterData['Meta.EV']; ?> ev</td>
                    <td class="index_main"> | f/<?php echo $masterData['Meta.Aperture']; ?></td>
                    <td class="index_main"> | <?php echo $masterData['Meta.Speed']; ?></td>
                </tr>
                </table>
            </div>
            <table class="index_full">
            <?php foreach ($masterData as $key => $value): ?>
                <?php if (strcmp(substr($key, 0, 5), 'Meta.')): ?>
                    <tr>
                        <td class="index_key"><?php echo $key;?> :</td><td class="index_value"><?php echo $value;?></td>
                    </tr>
                <?php endif; ?>
            <?php endforeach; ?>
        </table>
        </div>
    </div>
    <div class="column2" id="#column2">
        <?php
            $hrefProject = site_url('retrofocus/folder/'. $masterData['Meta.projectModelId']);
            $hrefMaster = str_replace('%', '%25', $masterData['Meta.imageFile']);
        ?>
        <div class="title" id="#title2"><a href="<?php
            echo site_url('retrofocus/library');?>">PROJECTS</a> / Project: <a href="<?php
            echo $hrefProject;?>"><?php
            echo $masterData['Meta.projectName'];?></a> / Master: <?php
            echo $title;?> / <div class="version_info" onclick="version_raw()"> {"info"} </div>
        </div>
        <div class="gallery" id="#gallery">
            <?php if (!strcmp($masterData['Meta.subtype'], 'JPGST') || !strcmp($masterData['Meta.subtype'], 'IMGST')): ?>
                <img class="image" src="<?php echo $hrefMaster; ?>">
            <?php else: ?>
                <a href="<?php echo $hrefMaster; ?>"><img class="image" src="/Retrofocus/images/UnsupportedImage_1800x1200.png"></a>
            <?php endif; ?>
        </div>
        <textarea readonly class="raw_viewer" id="#raw_viewer"><?php echo $masterData["Meta.BinaryData"]; ?></textarea>
    </div>
</div>
<script type="text/javascript">
    setViewersDimension();
    window.onresize = setViewersDimension;
</script>