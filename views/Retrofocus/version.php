<!-- Retrofocus V1.1 Copyright (C) 2020 eric.yape@gmail.com -->
<div class="folder" id="#folder">
    <div class="column1" id="#column1">
        <div class="title" id="#title1">Version sqlite Info</div>
        <div class="index" id="#index">
            <div class="index_short">
                <div class="index_main"><?php echo $versionData['Meta.Model']; ?></div>
                <div class="index_main"><?php echo $versionData['Meta.LensModel']; ?></div>
                <table class="index_mini">
                <tr>
                    <td class="index_main">ISO <?php echo $versionData['Meta.ISO'];?></td>
                    <td class="index_main"> | <?php echo $versionData['Meta.FocalLength']; ?></td>
                    <td class="index_main"> | <?php echo $versionData['Meta.EV']; ?> ev</td>
                    <td class="index_main"> | f/<?php echo $versionData['Meta.Aperture']; ?></td>
                    <td class="index_main"> | <?php echo $versionData['Meta.Speed']; ?></td>
                </tr>
                </table>
            </div>
            <table class="index_full">
            <?php foreach ($versionData as $key => $value): ?>
                <?php if (strcmp(substr($key, 0, 5), 'Meta.')): ?>
                    <tr>
                        <td class="index_key"><?php echo $key;?> :</td><td class="index_value"><?php echo $value;?></td>
                    </tr>
                <?php endif; ?>
            <?php endforeach; ?>
        </table>
        </div>
    </div>
    <div class="column2" id="#column2">
        <?php
            $hrefLibrary = site_url('retrofocus/library');
            $hrefProject = site_url('retrofocus/folder/'. $versionData['Meta.projectModelId']);
            $hrefImg = empty($versionData['Meta.fullSizePreviewPath']) ?
                "/Retrofocus/images/UnsupportedImage_1800x1200.png"
                :
                RF_LIBRARY_FOLDER . "/Previews/" . str_replace('%', '%25', $versionData['Meta.fullSizePreviewPath']);
        ?>
        <div class="title" id="#title2"><a href="<?php
            echo $hrefLibrary;?>">PROJECTS</a> / Project: <a href="<?php
            echo $hrefProject;?>"><?php echo $versionData['Meta.projectName'];?></a> / Version: <?php
            echo $title;?> / <div class="version_info" onclick="version_raw()"> {"info"} </div>
        </div>
        <div class="gallery" id="#gallery">
                <img class="image" src="<?php echo $hrefImg; ?>">
        </div>
        <textarea readonly class="raw_viewer" id="#raw_viewer"><?php echo $versionData["Meta.BinaryData"]; ?></textarea>
    </div>
</div>
<script type="text/javascript">
    setViewersDimension();
    window.onresize = setViewersDimension;
</script>